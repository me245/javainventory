package javainventory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javainventory.models.Inhouse;
import javainventory.models.Inventory;
import javainventory.models.Outsourced;
import javainventory.models.Part;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class PartController implements Initializable {
    
    @FXML private Label labelTitle;
    @FXML private Label labelMachineOrCompany;
    @FXML private RadioButton radioButtonInhouse;
    @FXML private RadioButton radioButtonOutsourced;
    @FXML private Button buttonSave;
    @FXML private Button buttonCancel;
    @FXML private TextField textFieldID;
    @FXML private TextField textFieldName;
    @FXML private TextField textFieldInv;
    @FXML private TextField textFieldPrice;
    @FXML private TextField textFieldMax;
    @FXML private TextField textFieldMachineOrCompany;
    @FXML private TextField textFieldMin;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO    
    }  
    
    @FXML
    public void actionButtonSave(){
        try {
            if(!checkLimits()) {
                JavaInventory.showError("Inventory must be between max and min and min can't be higher than max.\nPlease verify and try again");
                return;
            }           
        } catch (NumberFormatException e) {
            JavaInventory.showError("Fields for Inventory, Max, and Min may be malformated.\nPlease verify and try again");
            return;
        }
        try{ //try and parse fields
            Part part;
            int id;
            boolean update = false;
            boolean replace = false;
            if(!labelTitle.getText().equals("Modify Part")) {               
                id = Inventory.getNextPartID();
                part = newPart(id);
            } else {
                part = JavaInventory.getInventory().lookupPart(Integer.parseInt(textFieldID.getText()));
                if((part instanceof Inhouse && radioButtonOutsourced.isSelected()) || (part instanceof Outsourced && radioButtonInhouse.isSelected())) { //If type of part is switched during modify, create a new part masquerading as former part
                    if(JavaInventory.checkPart(part)) {
                        id = part.getPartID();
                        part = newPart(id);
                        replace = true;
                    } else {
                        JavaInventory.showError("Part Type switched and member of a product.\nCannot switch without addressing product Ownership");
                        return;
                    }
                } else {
                    update = true;
                }
            }
            part.setInStock(Integer.parseInt(textFieldInv.getText()));
            part.setName(textFieldName.getText());
            part.setMax(Integer.parseInt(textFieldMax.getText()));
            part.setMin(Integer.parseInt(textFieldMin.getText()));
            part.setPrice(Double.parseDouble(textFieldPrice.getText()));
            if(radioButtonInhouse.isSelected()) {
               ((Inhouse) part).setMachineID(Integer.parseInt(textFieldMachineOrCompany.getText()));
            } else {
                ((Outsourced) part).setCompanyName(textFieldMachineOrCompany.getText());    
            }
            if(replace) JavaInventory.getInventory().removePart(part.getPartID());
            if(!update) JavaInventory.getInventory().addPart(part);
            redirectHome();
        } catch(Exception e) {
            JavaInventory.showError("There was an error in trying to save this object.\nPlease check inputs and try again");
        }
    }
    
    @FXML
    public void actionButtonCancel() {
        if(JavaInventory.confirmDialog("Are you sure you want to cancel?\nAll Changes will be lost")) {
            redirectHome();
        }
    }
    
    public void setTitle(String titleString) {
        labelTitle.setText(titleString);
    }
    public void setInhouse() {
        labelMachineOrCompany.setText("Machine ID");
        radioButtonInhouse.setSelected(true);
    }
    public void setOutsource() {
        labelMachineOrCompany.setText("Company Name");
        radioButtonOutsourced.setSelected(true);
    }
    public void setID(String id) {
        textFieldID.setText(id);
    }
    public void setName(String name) {
        textFieldName.setText(name);
    }
    public void setInv(String inv) {
        textFieldInv.setText(inv);
    }
    public void setPrice(String price) {
        textFieldPrice.setText(price);
    }
    public void setMin(String min) {
        textFieldMin.setText(min);
    }
    public void setMax(String max) {
        textFieldMax.setText(max);
    }
    public void setMachineOrCompany(String titleString) {
        textFieldMachineOrCompany.setText(titleString);
    }
    
    //private helper method to check limits on form
    private boolean checkLimits() throws NumberFormatException {
        boolean passed = true;
        int min, max, inv;
        min = Integer.parseInt(textFieldMin.getText());
        max = Integer.parseInt(textFieldMax.getText());
        inv = Integer.parseInt(textFieldInv.getText());
        if(inv < min || inv > max || max < min) passed = false;
        return passed;
    }
    
    //helper method to generate appropriate new part
    private Part newPart(int id) {
        Part part;
        if(radioButtonInhouse.isSelected()) {
            part = new Inhouse();
        } else {
            part = new Outsourced();
        }
        part.setPartID(id);
        return part;
    }
    
    
    //redirect to main controller and scene
    private void redirectHome() {
        try{
            Parent root = FXMLLoader.load(getClass().getResource("/javainventory/Main.fxml"));

            Scene scene = new Scene(root);
            Stage stage = JavaInventory.getStage();

            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }   
}
