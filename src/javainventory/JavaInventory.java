package javainventory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javainventory.models.Inhouse;
import javainventory.models.Inventory;
import javainventory.models.Outsourced;
import javainventory.models.Part;
import javainventory.models.Product;
/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class JavaInventory extends Application {
    
    private static final Inventory INVENTORY = new Inventory();
    private static Stage stage;
    
    public static Inventory getInventory() {
        return INVENTORY;
    }
    
    public static Stage getStage() {
        return stage;
    }
    
    public static boolean confirmDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, message, ButtonType.YES, ButtonType.NO);
        alert.showAndWait();
        return alert.getResult() == ButtonType.YES;
    }
    
    public static void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.showAndWait();
    }
    
    //Not specified method, but ensures that a part isn't part of another
    //product before deleting that part
    public static boolean checkPart(Part part) {
        boolean safe = true;
        for (Product p : JavaInventory.getInventory().getProducts()) {
            if (p.getAssociatedParts().contains(part)) {
                if (JavaInventory.confirmDialog(part.getName() + " is a part of " + p.getName() + ".\nRemove from " + p.getName() + " to continue?")) {
                    p.removePart(part.getPartID());
                } else {
                    safe = false;
                }
            }
        }
        return safe;
    }
    
    //Adds inventory to ease in testing
    private static void seedInventory() {
        INVENTORY.addPart(new Outsourced("Part1", Inventory.getNextPartID(), 5.0, 1, 0, 5, "SomeCompany"));
        INVENTORY.addPart(new Outsourced("Part2", Inventory.getNextPartID(), 5.0, 1, 0, 5, "SomeCompany"));
        INVENTORY.addPart(new Inhouse("Part3", Inventory.getNextPartID(), 5.0, 1, 0, 5, 1));
        INVENTORY.addPart(new Outsourced("Part4", Inventory.getNextPartID(), 5.0, 1, 0, 5, "SomeCompany"));
        INVENTORY.addProduct(new Product("Product1", Inventory.getNextProductID(), 11.0, 1, 0, 0));
        INVENTORY.addProduct(new Product("Product2", Inventory.getNextProductID(), 12.0, 1, 0, 0));
        INVENTORY.addProduct(new Product("Product3", Inventory.getNextProductID(), 13.0, 1, 0, 0));
        INVENTORY.addProduct(new Product("Product4", Inventory.getNextProductID(), 14.0, 1, 0, 0));
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        //for testing
        //seedInventory();//Comment out for submission. Can uncomment to test.
        //end seed Inventory
        
        Parent root = FXMLLoader.load(getClass().getResource("/javainventory/Main.fxml"));
        
        Scene scene = new Scene(root);
        JavaInventory.stage = stage;

        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
