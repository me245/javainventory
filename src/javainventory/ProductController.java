package javainventory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javainventory.models.Inventory;
import javainventory.models.Part;
import javainventory.models.Product;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class ProductController implements Initializable {
    
     @FXML private TableView<Part> partTable;
     @FXML private TableView<Part> associatedPartTable;
     @FXML private TextField textFieldSearchPart;
     @FXML private TextField textFieldID;
     @FXML private TextField textFieldName;
     @FXML private TextField textFieldInv;
     @FXML private TextField textFieldPrice;
     @FXML private TextField textFieldMax;
     @FXML private TextField textFieldMin;
     @FXML private Button buttonAdd;
     @FXML private Button buttonDelete;
     @FXML private Label labelTitle;
    
    private int selectedPart = -1;    
    private int selectedAssociatedPart = -1;
    private boolean modifying = false;
    private final ObjectProperty<Predicate<Part>> partFilter = new SimpleObjectProperty<>();
    private final ArrayList<Part> partsToRemoveIfModifying = new ArrayList<>();
    private final ArrayList<Part> partsToAddIfModifying = new ArrayList<>();
    public ObservableList<Part> partList = FXCollections.observableArrayList(JavaInventory.getInventory().getAllParts());
    public ObservableList<Part> associatedPartList = FXCollections.observableArrayList(new ArrayList<>());

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        partTable.setItems(partList);
        associatedPartTable.setItems(associatedPartList);
        
        //create predicate for use in search function binding
        partFilter.bind(Bindings.createObjectBinding(() -> 
            part ->{
                return ( part.getName().toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()) ||
                        Integer.toString(part.getPartID()).toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()) ||
                        Integer.toString(part.getInStock()).toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()) ||
                        Double.toString(part.getPrice()).toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()));}, 
            textFieldSearchPart.textProperty()));
        
        //grab selection models for selecting items in table
        TableView.TableViewSelectionModel<Part> tvSelectPart = partTable.getSelectionModel();
        TableView.TableViewSelectionModel<Part> tvSelectAssociatedPart = associatedPartTable.getSelectionModel();
        tvSelectPart.selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> changed,
                    Number oldVal, Number newVal) {
                int index = (int) newVal;
                selectedPart = index;
                if (buttonAdd.isDisabled()) {
                    buttonAdd.setDisable(false);
                }
            }
        });
        tvSelectAssociatedPart.selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> changed,
                    Number oldVal, Number newVal) {
                int index = (int) newVal;
                selectedAssociatedPart = index;
                if (buttonDelete.isDisabled()) {
                    buttonDelete.setDisable(false);
                }
            }
        });
    }
    
    @FXML
    public void actionButtonDelete() {
        if(associatedPartList.size() < 2) {
            JavaInventory.showError("Cannot Delete Part.\nProduct Must Contain at least one Part");
            return;
        }
        if (!JavaInventory.confirmDialog("Are you sure you want to remove this part?")) {
            return;
        }
        Part part = associatedPartList.get(selectedAssociatedPart);
        associatedPartList.remove(selectedAssociatedPart);
        if (selectedAssociatedPart >= associatedPartList.size()) selectedAssociatedPart--;
        if(modifying) {
            partsToRemoveIfModifying.add(part);
        }
    }
    
    @FXML
    public void actionButtonSearch() {
        if (textFieldSearchPart.getText().equals("")) {
            partTable.setItems(partList);
            return;
        }
        FilteredList<Part> filteredParts = new FilteredList<>(partList);
        filteredParts.predicateProperty().bind(partFilter); //bind preexisting predicate
        partTable.setItems(filteredParts);
        filteredParts.predicateProperty().unbind();
        textFieldSearchPart.clear();
        if(selectedPart == 0 && partTable.getItems().size() > 0) return; //No need to clear selections if first one is still selected
        clearSelectedPart();
    }
    
    @FXML
    public void actionButtonAdd() {
        Part part = partList.get(selectedPart);
        if(associatedPartList.contains(part)) {
            JavaInventory.showError("Product contains this part already");
            return;
        }
        associatedPartList.add(part);
        if(modifying) {
            partsToAddIfModifying.add(part);
        }
    }

    @FXML
    public void actionButtonSave() {
        if(associatedPartList.size() < 1) {
            JavaInventory.showError("Product Must have a Part associated.\nPlease check and try again");
            return;
        }       
        try {
            if (!checkLimits()) {
                return;
            }
        } catch (NumberFormatException e) {
            JavaInventory.showError("Fields for Inventory, Max, Min, and Price may be malformated.\nPlease verify and try again");
            return;
        }
        Product product;
        if(modifying) {
            product = JavaInventory.getInventory().lookupProduct(Integer.parseInt(textFieldID.getText()));
        } else {
            product = new Product();
            product.setProductID(Inventory.getNextProductID());
        }
        try {
            product.setName(textFieldName.getText());
            product.setPrice(Double.parseDouble(textFieldPrice.getText()));
            product.setInStock(Integer.parseInt(textFieldInv.getText()));
            product.setMin(Integer.parseInt(textFieldMin.getText()));
            product.setMax(Integer.parseInt(textFieldMax.getText()));
            if(!modifying) {
                associatedPartList.forEach(p -> product.addPart(p));
                JavaInventory.getInventory().addProduct(product);
            } else {
                partsToRemoveIfModifying.forEach(p -> product.removePart(p.getPartID()));
                partsToAddIfModifying.forEach(p -> product.addPart(p));
            }
            redirectHome();
        } catch (Exception e) {
            JavaInventory.showError("There was an error saving.\nPlease check input fields and try again");
        }
    }

    @FXML
    public void actionButtonCancel() {
        if(JavaInventory.confirmDialog("Are you sure you want to cancel?\nAll Changes will be lost")) {
            redirectHome();
        }
    }
    
    //Public method to switch to modifying mode and to populate fields.
    public void modifyExisting(Product product) {
        modifying = true;
        product.getAssociatedParts().forEach((p) -> associatedPartList.add(p));
        labelTitle.setText("Modify Product");
        textFieldID.setText(((Integer) (product.getProductID())).toString());
        textFieldName.setText(product.getName());
        textFieldInv.setText(((Integer) (product.getInStock())).toString());
        textFieldPrice.setText(((Double) (product.getPrice())).toString());
        textFieldMax.setText(((Integer) (product.getMax())).toString());
        textFieldMin.setText(((Integer) (product.getMin())).toString());
    }
    
    
    //redirects to home controller and scene
    private void redirectHome() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/javainventory/Main.fxml"));

            Scene scene = new Scene(root);
            Stage stage = JavaInventory.getStage();

            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //sync up selections after search
    private void clearSelectedPart() {
        selectedPart = -1;
        buttonAdd.setDisable(true);
    }
    
    //check field constraints
    private boolean checkLimits() throws NumberFormatException {
        boolean passed = true;
        int min, max, inv;
        double price;
        double priceOfParts = 0.0;
        min = Integer.parseInt(textFieldMin.getText());
        max = Integer.parseInt(textFieldMax.getText());
        inv = Integer.parseInt(textFieldInv.getText());
        price = Double.parseDouble(textFieldPrice.getText());
        if (inv < min || inv > max || max < min) {
            passed = false;
            JavaInventory.showError("Inventory must be between max and min and min can't be higher than max.\nPlease verify and try again");
        }
        priceOfParts = associatedPartList.stream().map((p) -> p.getPrice()).reduce(priceOfParts, (accumulator, _item) -> accumulator + _item);
        if (price < priceOfParts) {
            passed = false;
            JavaInventory.showError("Price must be higher than the sum of the prices of all parts.\nPlease verify and try again");
        }
        return passed;
    }
}
