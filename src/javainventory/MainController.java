package javainventory;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javainventory.models.Inhouse;
import javainventory.models.Outsourced;
import javainventory.models.Part;
import javainventory.models.Product;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class MainController implements Initializable {
    
    @FXML private TableView<Part> partTable;
    @FXML private TableView<Product> productTable;
    @FXML private Button buttonAddPart;
    @FXML private Button buttonAddProduct;
    @FXML private Button buttonSearchPart;
    @FXML private Button buttonSearchProduct;
    @FXML private Button buttonModifyPart;
    @FXML private Button buttonModifyProduct;
    @FXML private Button buttonDeletePart;
    @FXML private Button buttonDeleteProduct;
    @FXML private Button buttonExit;
    @FXML private TextField textFieldSearchPart;
    @FXML private TextField textFieldSearchProduct;
      
    private int selectedPart = -1;    
    private int selectedProduct = -1;
    private final ObjectProperty<Predicate<Part>> partFilter = new SimpleObjectProperty<>();
    private final ObjectProperty<Predicate<Product>> productFilter = new SimpleObjectProperty<>();    
    public ObservableList<Part> partList = FXCollections.observableArrayList(JavaInventory.getInventory().getAllParts());       
    public ObservableList<Product> productList = FXCollections.observableArrayList(JavaInventory.getInventory().getProducts());
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        reinitializePartsAndProducts();
        partTable.setItems(partList);
        productTable.setItems(productList);
        
        //Adds a predicateBinding to object property to later bind in search function
        partFilter.bind(Bindings.createObjectBinding(() -> 
            part ->{
                return ( part.getName().toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()) ||
                        Integer.toString(part.getPartID()).toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()) ||
                        Integer.toString(part.getInStock()).toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()) ||
                        Double.toString(part.getPrice()).toLowerCase().contains(textFieldSearchPart.getText().toLowerCase()));}, 
            textFieldSearchPart.textProperty()));
        
        productFilter.bind(Bindings.createObjectBinding(() -> 
            product ->{
                return ( product.getName().toLowerCase().contains(textFieldSearchProduct.getText().toLowerCase()) ||
                        Integer.toString(product.getProductID()).toLowerCase().contains(textFieldSearchProduct.getText().toLowerCase()) ||
                        Integer.toString(product.getInStock()).toLowerCase().contains(textFieldSearchProduct.getText().toLowerCase()) ||
                        Double.toString(product.getPrice()).toLowerCase().contains(textFieldSearchProduct.getText().toLowerCase()));}, 
            textFieldSearchProduct.textProperty()));
        
        
        //Obtain selection model for tables
        TableView.TableViewSelectionModel<Part> tvSelectPart = partTable.getSelectionModel();
        TableView.TableViewSelectionModel<Product> tvSelectProduct = productTable.getSelectionModel();
        tvSelectPart.selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> changed,
                    Number oldVal, Number newVal) {
                int index = (int)newVal;
                selectedPart = index;
                if(buttonModifyPart.isDisabled()){
                    buttonModifyPart.setDisable(false);
                }
                if(buttonDeletePart.isDisabled()){
                    buttonDeletePart.setDisable(false);
                }
            }
        });
        tvSelectProduct.selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> changed,
                    Number oldVal, Number newVal) {
                int index = (int)newVal;
                selectedProduct = index;
                if(buttonModifyProduct.isDisabled()){
                    buttonModifyProduct.setDisable(false);
                }
                if(buttonDeleteProduct.isDisabled()){
                    buttonDeleteProduct.setDisable(false);
                }
            }
        });
    }
    
    @FXML
    public void exitBtnAction() {
        if (JavaInventory.confirmDialog("Are you sure you want to exit?\nAll Changes will be lost")) {
            Platform.exit();
        }
    }
    
    @FXML
    public void addPartBtnAction() {
        partFormLauncher(null);
    }

    @FXML
    public void addProductBtnAction() {
        productFormLauncher(null);
    }
    
    @FXML
    public void searchPartBtnAction() {
        if(textFieldSearchPart.getText().equals("")) {
            partTable.setItems(partList);
            return;
        }
        FilteredList<Part> filteredParts = new FilteredList<>(partList);
        filteredParts.predicateProperty().bind(partFilter);//Binds predicate for searching
        partTable.setItems(filteredParts);
        filteredParts.predicateProperty().unbind();
        textFieldSearchPart.clear();
        if(selectedPart == 0 && partTable.getItems().size() > 0) return; //No need to clear selections if first one is still selected
        clearSelectedPart();
    }
    
    @FXML
    public void searchProductBtnAction() {
        if(textFieldSearchProduct.getText().equals("")) {
            productTable.setItems(productList);
            return;
        }
        FilteredList<Product> filteredProducts = new FilteredList<>(productList);
        filteredProducts.predicateProperty().bind(productFilter);
        productTable.setItems(filteredProducts);
        filteredProducts.predicateProperty().unbind();
        textFieldSearchProduct.clear();
        if(selectedProduct == 0 && productTable.getItems().size() > 0) return; //No need to clear selections if first one is still selected
        clearSelectedProduct();
    }
    
    @FXML
    public void modifyPartBtnAction() {
        partFormLauncher(partTable.getItems().get(selectedPart));
    }
    
    @FXML
    public void modifyProductBtnAction() {
        productFormLauncher(productTable.getItems().get(selectedProduct));
    }
    
    @FXML
    public void deletePartBtnAction() {
        Part part = partTable.getItems().get(selectedPart);
        if (JavaInventory.confirmDialog("Are you sure you want to delete " + part.getName() + "?")) {
            if(JavaInventory.checkPart(part)) {
                int index = part.getPartID();
                JavaInventory.getInventory().removePart(index);
                partList.remove(selectedPart);
                if(selectedPart >= partList.size()) selectedPart--;
                if(selectedPart < 0) {
                    clearSelectedPart();
                } 
            }
        }
    }
    
    @FXML
    public void deleteProductBtnAction() {
        Product product = productTable.getItems().get(selectedProduct);
        if (JavaInventory.confirmDialog("Are you sure you want to delete " + product.getName() + "?")) {
            if(product.getAssociatedParts().size() > 0) {
               if(!JavaInventory.confirmDialog(product.getName() + " has Parts assigned to it.\nAre you sure you want to delete?")) {
                   return;
               } 
            }
            int index = product.getProductID();
            JavaInventory.getInventory().removeProduct(index);
            productList.remove(selectedProduct);
            if(selectedProduct >= productList.size()) selectedProduct--;
            if(index < 0) {
                clearSelectedProduct();
            }
        }
    }
    
    //Used to sync controller data up with Inventory Model for application
    private void reinitializePartsAndProducts() {
        partList = FXCollections.observableArrayList(JavaInventory.getInventory().getAllParts());
        productList = FXCollections.observableArrayList(JavaInventory.getInventory().getProducts());
    }
    
    //Switches the scene to part scene and if needed, populate fields
    private void partFormLauncher(Part part) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/javainventory/Part.fxml"));
            root = loader.load();
            PartController controller = loader.getController();
            if (part != null) {
                setPartControllerFields(controller, part);
            }
            Scene scene = new Scene(root);
            Stage stage = JavaInventory.getStage();
            stage.setTitle("Part Form");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    //Makes use of public setters to populate fields on partController
    private void setPartControllerFields(PartController controller, Part part) {
        controller.setTitle("Modify Part");
        if (part instanceof Inhouse) {
            controller.setInhouse();
            controller.setMachineOrCompany(Integer.toString(((Inhouse) part).getMachineID()));
        } else {
            controller.setOutsource();
            controller.setMachineOrCompany(((Outsourced) part).getCompanyName());
        }
        controller.setID(Integer.toString(part.getPartID()));
        controller.setName(part.getName());
        controller.setInv(Integer.toString(part.getInStock()));
        controller.setPrice(Double.toString(part.getPrice()));
        controller.setMax(Integer.toString(part.getMax()));
        controller.setMin(Integer.toString(part.getMin()));
    }

    //Used to switch scene to Product scene. If necessary will use a public
    //method to pass in a product to modify
    private void productFormLauncher(Product product) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/javainventory/Product.fxml"));
            root = loader.load();
            ProductController controller = loader.getController();
            if (product != null) {
                controller.modifyExisting(product);
            }
            Scene scene = new Scene(root);
            Stage stage = JavaInventory.getStage();
            stage.setTitle("Product Form");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Sync up selection indexes after search
    private void clearSelectedPart() {
        selectedPart = -1;
        buttonModifyPart.setDisable(true);
        buttonDeletePart.setDisable(true);
    }

    //Sync up selection indexes after search
    private void clearSelectedProduct() {
        selectedProduct = -1;
        buttonModifyProduct.setDisable(true);
        buttonDeleteProduct.setDisable(true);
    }
}
