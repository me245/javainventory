package javainventory.models;

/**
 * Created by Michael Evanson on 5/18/2017.
 */

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;

public abstract class Part {
    
    private final SimpleStringProperty name = new SimpleStringProperty("");
    private final SimpleIntegerProperty partID = new SimpleIntegerProperty(-1);
    private final SimpleDoubleProperty price = new SimpleDoubleProperty(0);
    private final SimpleIntegerProperty inStock = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty min = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty max = new SimpleIntegerProperty(0);

    public Part() {
        this("", -1, 0, 0, 0, 0);
    }

    public Part(String name, int ID, double price, int inStock, int min, int max) {
        this.partID.set(ID);
        this.name.set(name);
        this.price.set(price);
        this.inStock.set(inStock);
        this.min.set(min);
        this.max.set(max);
    }

    public String getName() {
        return name.get();
    }

    public int getPartID() {
        return partID.get();
    }

    public void setPartID(int partID) {
        this.partID.set(partID);
    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getInStock() {
        return inStock.get();
    }

    public void setInStock(int inStock) {
        this.inStock.set(inStock);
    }

    public int getMin() {
        return min.get();
    }

    public void setMin(int min) {
        this.min.set(min);
    }

    public int getMax() {
        return max.get();
    }

    public void setMax(int max) {
        this.max.set(max);
    }
}
