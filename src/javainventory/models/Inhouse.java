package javainventory.models;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class Inhouse extends Part {
    
    private final SimpleIntegerProperty machineID = new SimpleIntegerProperty(-1);
    
    public Inhouse() {
        this("", -1, 0, 0, 0, 0, 0);
    }
    
    public Inhouse(String name, int ID, double price, int instock, int min, int max, int machineID) {
        super(name, ID, price, instock, min, max);
        this.machineID.set(machineID);
    }
    
    public int getMachineID() {
        return machineID.get();
    }

    public void setMachineID(int machineID) {
        this.machineID.set(machineID);
    }
}
