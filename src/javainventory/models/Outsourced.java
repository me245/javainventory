package javainventory.models;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
import javafx.beans.property.SimpleStringProperty;

public class Outsourced extends Part {

    private final SimpleStringProperty companyName = new SimpleStringProperty("");

    public Outsourced() {
        this("", -1, 0, 0, 0, 0, "");
    }

    public Outsourced(String name, int ID, double price, int instock, int min, int max, String companyName) {
        super(name, ID, price, instock, min, max);
        this.companyName.set(companyName);
    }

    public String getCompanyName() {
        return companyName.get();
    }

    public void setCompanyName(String companyName) {
        this.companyName.set(companyName);
    }
}
