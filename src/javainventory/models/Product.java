package javainventory.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class Product {

    private final ArrayList<Part> associatedParts = new ArrayList<>();
    private final SimpleIntegerProperty productID = new SimpleIntegerProperty(-1);
    private final SimpleStringProperty name = new SimpleStringProperty("");
    private final SimpleDoubleProperty price = new SimpleDoubleProperty(0);
    private final SimpleIntegerProperty inStock = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty min = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty max = new SimpleIntegerProperty(0);
    public static Part selectedPart = null;

    public Product() {
        this("", 0, 0, 0, 0, 0);
    }

    public Product(String name, int ID, double price, int inStock, int min, int max) {
        this.productID.set(ID);
        this.name.set(name);
        this.price.set(price);
        this.inStock.set(inStock);
        this.min.set(min);
        this.max.set(max);
    }

    public ArrayList<Part> getAssociatedParts() {
        return associatedParts;
    }

    public int getProductID() {
        return productID.get();
    }

    public void setProductID(int productID) {
        this.productID.set(productID);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public int getInStock() {
        return inStock.get();
    }

    public void setInStock(int inStock) {
        this.inStock.set(inStock);
    }

    public int getMin() {
        return min.get();
    }

    public void setMin(int min) {
        this.min.set(min);
    }

    public int getMax() {
        return max.get();
    }

    public void setMax(int max) {
        this.max.set(max);
    }

    public static Part getSelectedPart() {
        return selectedPart;
    }

    public void addPart(Part part) {
        associatedParts.add(part);
    }

    public boolean removePart(int partID) {
        if (associatedParts.size() < 1) {
            return false;
        }
        partSort();
        int index = searchPart(partID);
        if (index < 0) {
            return false;
        }
        associatedParts.remove(index);
        return true;
    }
    //Returns the part given a partID
    public Part lookupPart(int partID) {
        if (associatedParts.size() < 1) {
            throw new IndexOutOfBoundsException("This Product does not contain any parts to lookup");
        }
        partSort();
        int index = searchPart(partID);
        if (index < 0) {
            throw new IndexOutOfBoundsException("This Product does not contain this part");
        }
        return associatedParts.get(index);
    }
    //Given a partID makes a part statically available to modify, if not handled in controllers
    public void updatePart(int partID) {
        if (associatedParts.size() < 1) {
            Product.selectedPart = null;
            return;
        }
        partSort();
        int index = searchPart(partID);
        if (index < 0) {
            Product.selectedPart = null;
            return;
        }
        Product.selectedPart = associatedParts.get(index);
    }
    //Sorts associatedParts according to partID
    private void partSort() {
        Collections.sort(associatedParts, new Comparator<Part>() {
            @Override
            public int compare(Part p1, Part p2) {
                return p1.getPartID() - p2.getPartID();
            }
        });
    }
    //Performs binary search on a sorted Associated Parts to retrieve index in array
    private int searchPart(int partID) {
        int index, start = 0, end = associatedParts.size() - 1;
        index = end / 2;
        int left = start;
        int right = end;
        while (index <= right && index >= left) {
            if (index > end || index < start) {
                break;
            }
            int test = associatedParts.get(index).getPartID() - partID;
            if (test == 0) {
                return index;
            } else if (test > 0) {
                right = index;
                index /= 2;
            } else {
                left = index;
                index += ((end - index) / 2) > 1 ? (end - index) / 2 : 1;
            }
        }
        return -1;
    }
}