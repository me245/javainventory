package javainventory.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Michael Evanson on 5/18/2017.
 */
public class Inventory {

    public static Product productSelected = null;
    private static int productID = 0;
    public static Part partSelected = null;
    private static int partID = 0;

    public static int getNextPartID() {
        return partID++;
    }

    public static int getNextProductID() {
        return productID++;
    }

    public static Part getSelectedPart() {
        return partSelected;
    }

    public static Product getSelectedProduct() {
        return productSelected;
    }

    private final ArrayList<Product> products = new ArrayList<>();
    private final ArrayList<Part> allParts = new ArrayList<>();

    public Inventory() {
    }

    public ArrayList<Part> getAllParts() {
        return allParts;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public boolean removeProduct(int productID) {
        if (products.size() < 1) {
            return false;
        }
        productSort();
        int index = searchProduct(productID);
        if (index < 0) {
            return false;
        }
        products.remove(index);
        return true;
    }

    public Product lookupProduct(int productID) {
        if (products.size() < 1) {
            throw new IndexOutOfBoundsException("Inventory does not contain any Products");
        }
        productSort();
        int index = searchProduct(productID);
        if (index < 0) {
            throw new IndexOutOfBoundsException("Inventory does not contain this Product");
        }
        return products.get(index);
    }

    public void updateProduct(int productID) {
        if (products.size() < 1) {
            Inventory.productSelected = null;
            return;
        }
        productSort();
        int index = searchProduct(productID);
        if (index < 0) {
            Inventory.productSelected = null;
            return;
        }
        Inventory.productSelected = products.get(index);
    }

    public void addPart(Part part) {
        allParts.add(part);
    }

    public boolean removePart(int partID) {
        if (allParts.size() < 1) {
            return false;
        }
        partSort();
        int index = searchPart(partID);
        if (index < 0) {
            return false;
        }
        allParts.remove(index);
        return true;
    }

    public Part lookupPart(int partID) {
        if (allParts.size() < 1) {
            throw new IndexOutOfBoundsException("This Product does not contain any parts to lookup");
        }
        partSort();
        int index = searchPart(partID);
        if (index < 0) {
            throw new IndexOutOfBoundsException("This Product does not contain this part");
        }
        return allParts.get(index);
    }

    public void updatePart(int partID) {
        if (allParts.size() < 1) {
            partSelected = null;
            return;
        }
        partSort();
        int index = searchPart(partID);
        if (index < 0) {
            partSelected = null;
            return;
        }
        Product.selectedPart = allParts.get(index);
    }

    private int searchProduct(int productID) {
        int index, start = 0, end = products.size() - 1;
        index = end / 2;
        int left = start;
        int right = end;
        while (index <= right && index >= left) {
            if (index > end || index < start) {
                break;
            }
            int test = products.get(index).getProductID() - productID;
            if (test == 0) {
                return index;
            } else if (test > 0) {
                right = index;
                index /= 2;
            } else {
                left = index;
                index += ((end - index) / 2) > 1 ? (end - index) / 2 : 1;
            }
        }
        return -1;
    }

    private void productSort() {
        Collections.sort(products, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return p1.getProductID() - p2.getProductID();
            }
        });
    }

    private void partSort() {
        Collections.sort(allParts, new Comparator<Part>() {
            @Override
            public int compare(Part p1, Part p2) {
                return p1.getPartID() - p2.getPartID();
            }
        });
    }

    private int searchPart(int partID) {
        int index, start = 0, end = allParts.size() - 1;
        index = end / 2;
        int left = start;
        int right = end;
        while (index <= right && index >= left) {
            if (index > end || index < start) {
                break;
            }
            int test = allParts.get(index).getPartID() - partID;
            if (test == 0) {
                return index;
            } else if (test > 0) {
                right = index;
                index /= 2;
            } else {
                left = index;
                index += ((end - index) / 2) > 1 ? (end - index) / 2 : 1;
            }
        }
        return -1;
    }
}
